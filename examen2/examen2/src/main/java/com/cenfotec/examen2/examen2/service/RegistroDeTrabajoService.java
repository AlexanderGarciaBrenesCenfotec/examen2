package com.cenfotec.examen2.examen2.service;

import com.cenfotec.examen2.examen2.domain.RegistroDeTrabajo;
import com.cenfotec.examen2.examen2.repository.RegistroDeTrabajoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegistroDeTrabajoService {
    @Autowired
    RegistroDeTrabajoRepository registroDeTrabajoRepository;
    public void saveRegistroDeTrabajo(RegistroDeTrabajo registroDeTrabajo){
//        Author nuevo = new Author();
//        nuevo.setName("Cualquiera " + Instant.now().getEpochSecond());
//
//        List<Author> authors = new ArrayList<>();
//        authors.add(nuevo);
//
//        registroDeTrabajo.setAuthors(authors);

        registroDeTrabajoRepository.save(registroDeTrabajo);
    }

    public List<RegistroDeTrabajo> getAll(){
        return registroDeTrabajoRepository.findAll();
    }


    public Optional<RegistroDeTrabajo> getById(int id){
        return registroDeTrabajoRepository.findById(Long.valueOf(id));
    }

    public void updateRegistroDeTrabajo(RegistroDeTrabajo registroDeTrabajo){
        registroDeTrabajoRepository.save(registroDeTrabajo);
    }

    public void deleteRegistroDeTrabajo(int id){
        registroDeTrabajoRepository.deleteById(Long.valueOf(id));
    }
}
