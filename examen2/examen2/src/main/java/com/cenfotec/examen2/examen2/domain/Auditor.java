package com.cenfotec.examen2.examen2.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Auditor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String direccion;
    private String telefono;
    private Date fechaNacimiento;
    private String email;
    private Boolean disponibilidadViajes;
    private String especialidad;
    private Boolean activo;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cliente")
    private RegistroDeTrabajo registroDeTrabajo;

    @Transient
    private SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");

    public Auditor() {
    }

    public Auditor(String nombre, String apellidoUno, String apellidoDos,
                    String direccion, String telefono, String fechaNacimiento, String email,
                    Boolean disponibilidadViajes, String especialidad, Boolean activo) throws ParseException {
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNacimiento = format.parse(fechaNacimiento);
        this.email = email;
        this.disponibilidadViajes = disponibilidadViajes;
        this.especialidad = especialidad;
        this.activo = activo;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }
    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }
    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getDisponibilidadViajes() {
        return disponibilidadViajes;
    }
    public void setDisponibilidadViajes(Boolean disponibilidadViajes) {
        this.disponibilidadViajes = disponibilidadViajes;
    }

    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public Boolean getActivo() {
        return activo;
    }
    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getFechaNacimientoAsShort() {
        return format.format(fechaNacimiento);
    }
}
