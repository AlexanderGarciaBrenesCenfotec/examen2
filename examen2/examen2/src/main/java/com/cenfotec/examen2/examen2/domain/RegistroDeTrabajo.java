package com.cenfotec.examen2.examen2.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class RegistroDeTrabajo {
    //Auditoria, cliente, fecha, auditor, area
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String areaDeConsulta;
    private Date fechaDeConsulta;
    private String auditoria;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AUDITOR_ID")
    @JsonBackReference
    private Auditor auditor;

//    @OneToOne
//    @JoinColumn(name = "CLIENTE_ID", referencedColumnName = "ID")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CLIENTE_ID")
    @JsonBackReference
    private Cliente cliente;

    @Transient
    private SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");

    public RegistroDeTrabajo() {
    }

    public RegistroDeTrabajo(Long id, String areaDeConsulta, Date fechaDeConsulta, String auditoria) {
        this.id = id;
        this.areaDeConsulta = areaDeConsulta;
        this.fechaDeConsulta = fechaDeConsulta;
        this.auditoria = auditoria;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getAreaDeConsulta() {
        return areaDeConsulta;
    }
    public void setAreaDeConsulta(String areaDeConsulta) {
        this.areaDeConsulta = areaDeConsulta;
    }

    public Date getFechaDeConsulta() {
        return fechaDeConsulta;
    }
    public void setFechaDeConsulta(Date fechaDeConsulta) {
        this.fechaDeConsulta = fechaDeConsulta;
    }

    public Auditor getAuditor() {
        return auditor;
    }
    public void setAuditor(Auditor auditor) {
        this.auditor = auditor;
    }

    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getAuditoria() {
        return auditoria;
    }
    public void setAuditoria(String auditoria) {
        this.auditoria = auditoria;
    }

    public String getFechaDeConsultaAsShort() {
        return format.format(fechaDeConsulta);
    }
}
