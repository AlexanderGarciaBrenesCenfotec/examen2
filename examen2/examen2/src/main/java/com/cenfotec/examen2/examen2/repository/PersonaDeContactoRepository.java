package com.cenfotec.examen2.examen2.repository;

import com.cenfotec.examen2.examen2.domain.PersonaDeContacto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaDeContactoRepository extends JpaRepository<PersonaDeContacto, Long> {
}
