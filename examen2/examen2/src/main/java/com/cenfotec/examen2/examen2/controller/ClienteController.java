package com.cenfotec.examen2.examen2.controller;

import com.cenfotec.examen2.examen2.domain.Cliente;
import com.cenfotec.examen2.examen2.repository.PersonaDeContactoRepository;
import com.cenfotec.examen2.examen2.service.ClienteService;
import com.cenfotec.examen2.examen2.service.PersonaDeContactoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class ClienteController {
    @Autowired
    ClienteService clienteService;
    @Autowired
    PersonaDeContactoService personaDeContactoService;

    @RequestMapping("/mostrarClientes")
    public String index(Model model){
        model.addAttribute("cliente", clienteService.getAll());
        model.addAttribute("personaDeContacto", personaDeContactoService.getAll());
        return "mostrarClientes";
    }

//    @RequestMapping("/agregarPersonaDeContactoCliente")
//    public String personaDeContactoCliente(Model model){
//        model.addAttribute("cliente", clienteService.getAll());
//        return "mostrarClientes";
//    }

    @RequestMapping(value = "/agregarCliente", method = RequestMethod.GET)
    public String navegarPaginaInsertar(Model model){
        model.addAttribute(new Cliente());
        model.addAttribute("personaDeContacto", personaDeContactoService.getAll());

        return "agregarCliente";
    }

    @RequestMapping(value = "/agregarCliente", method = RequestMethod.POST)
    public String accionPaginaInsertar(Cliente cliente, BindingResult result, Model model){
        //cliente.setCreated(Date.from(Instant.now()));
        clienteService.saveCliente(cliente);


        return "exito";
    }

    @RequestMapping(value = "/editarCliente/{id}")
    public String irAEditar(Model model, @PathVariable int id) {
        Optional<Cliente> clienteToEdit = clienteService.getById(id);
        if (clienteToEdit.isPresent()){
            model.addAttribute("clienteToEdit", clienteToEdit);
            model.addAttribute("personaDeContacto", personaDeContactoService.getAll());
            return "editarCliente";
        } else {
            return "notFound";
        }
    }

    @RequestMapping(value = "/editarCliente/{id}", method = RequestMethod.POST)
    public String guardarCambios(Cliente cliente, BindingResult result,Model model,
                                 @PathVariable int id) {
        //cliente.setCreated(Date.from(Instant.now())); // esto normalmente
        //podria ir en el service.
        clienteService.updateCliente(cliente);
        return "exito";
    }


//    @RequestMapping(value = "/borrarCliente/{id}")
//    public String borrar(Model model, @PathVariable int id){
//        clienteService.deleteCliente(id);
//
//        return "exito";
//    }
}
