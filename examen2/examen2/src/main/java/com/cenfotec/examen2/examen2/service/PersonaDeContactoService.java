package com.cenfotec.examen2.examen2.service;

import com.cenfotec.examen2.examen2.domain.PersonaDeContacto;
import com.cenfotec.examen2.examen2.repository.PersonaDeContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaDeContactoService {
    @Autowired
    PersonaDeContactoRepository personaDeContactoRepository;
    public void savePersonaDeContacto(PersonaDeContacto personaDeContacto){
//        Author nuevo = new Author();
//        nuevo.setName("Cualquiera " + Instant.now().getEpochSecond());
//
//        List<Author> authors = new ArrayList<>();
//        authors.add(nuevo);
//
//        personaDeContacto.setAuthors(authors);

        personaDeContactoRepository.save(personaDeContacto);
    }

    public List<PersonaDeContacto> getAll(){
        return personaDeContactoRepository.findAll();
    }


    public Optional<PersonaDeContacto> getById(int id){
        return personaDeContactoRepository.findById(Long.valueOf(id));
    }

    public void updatePersonaDeContacto(PersonaDeContacto personaDeContacto){
        personaDeContactoRepository.save(personaDeContacto);
    }

    public void deletePersonaDeContacto(int id){
        personaDeContactoRepository.deleteById(Long.valueOf(id));
    }
}
