package com.cenfotec.examen2.examen2.controller;

import com.cenfotec.examen2.examen2.domain.Cliente;
import com.cenfotec.examen2.examen2.domain.PersonaDeContacto;
import com.cenfotec.examen2.examen2.service.ClienteService;
import com.cenfotec.examen2.examen2.service.PersonaDeContactoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class PersonaDeContactoController {
    @Autowired
    PersonaDeContactoService personaDeContactoService;

    @Autowired
    ClienteService clienteService;

    @RequestMapping("/mostrarPersonaDeContacto")
    public String index(Model model){
        model.addAttribute("personaDeContacto", personaDeContactoService.getAll());
        return "mostrarPersonaDeContacto";
    }

    @RequestMapping(value = "/agregarPersonaDeContacto", method = RequestMethod.GET)
    public String navegarPaginaInsertar(Model model){
        model.addAttribute(new PersonaDeContacto());
        return "agregarPersonaDeContacto";
    }

    @RequestMapping(value = "/agregarPersonaDeContacto", method = RequestMethod.POST)
    public String accionPaginaInsertar(PersonaDeContacto personaDeContacto, BindingResult result, Model model){
        //personaDeContacto.setFechaNacimiento(Date.from(Instant.now()));
        personaDeContactoService.savePersonaDeContacto(personaDeContacto);

        return "exito";
    }

    @RequestMapping(value = "/editarPersonaDeContacto/{id}")
    public String irAEditar(Model model, @PathVariable int id) {
        Optional<PersonaDeContacto> personaDeContactoToEdit = personaDeContactoService.getById(id);
        if (personaDeContactoToEdit.isPresent()){
            model.addAttribute("personaDeContactoToEdit", personaDeContactoToEdit);
            return "editarPersonaDeContacto";
        } else {
            return "notFound";
        }
    }

    @RequestMapping(value = "/editarPersonaDeContacto/{id}", method = RequestMethod.POST)
    public String guardarCambios(PersonaDeContacto personaDeContacto, BindingResult result,Model model,
                                 @PathVariable int id) {
        //personaDeContacto.setFechaNacimiento(Date.from(Instant.now())); // esto normalmente podria ir en el service.
        personaDeContactoService.updatePersonaDeContacto(personaDeContacto);
        return "exito";
    }


    @RequestMapping(value = "/borrarPersonaDeContacto/{id}")
    public String borrar(Model model, @PathVariable int id){
        List<Cliente> clientes = clienteService.getAll();

        for (Cliente c : clientes) {
            for (PersonaDeContacto p : c.getPersonasDeContacto()) {
                if(p.getId() == id){
                    c.getPersonasDeContacto().remove(p);
                }
            }
        }

        personaDeContactoService.deletePersonaDeContacto(id);

        return "exito";
    }
}
