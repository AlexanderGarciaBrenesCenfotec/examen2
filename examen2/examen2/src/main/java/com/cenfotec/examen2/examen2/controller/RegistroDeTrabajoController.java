package com.cenfotec.examen2.examen2.controller;

import com.cenfotec.examen2.examen2.domain.RegistroDeTrabajo;
import com.cenfotec.examen2.examen2.service.AuditorService;
import com.cenfotec.examen2.examen2.service.ClienteService;
import com.cenfotec.examen2.examen2.service.RegistroDeTrabajoService;
import com.cenfotec.examen2.examen2.service.PersonaDeContactoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@Controller
public class RegistroDeTrabajoController {
    @Autowired
    RegistroDeTrabajoService registroDeTrabajoService;
    @Autowired
    ClienteService clienteService;
    @Autowired
    AuditorService auditorService;

    @RequestMapping("/mostrarRegistroDeTrabajo")
    public String index(Model model){
        model.addAttribute("registroDeTrabajo", registroDeTrabajoService.getAll());
        model.addAttribute("cliente", clienteService.getAll());
        model.addAttribute("auditor", auditorService.getAll());

        return "mostrarRegistroDeTrabajo";
    }

//    @RequestMapping("/agregarPersonaDeContactoRegistroDeTrabajo")
//    public String personaDeContactoRegistroDeTrabajo(Model model){
//        model.addAttribute("registroDeTrabajo", registroDeTrabajoService.getAll());
//        return "mostrarRegistroDeTrabajos";
//    }

    @RequestMapping(value = "/agregarRegistroDeTrabajo", method = RequestMethod.GET)
    public String navegarPaginaInsertar(Model model){
        model.addAttribute(new RegistroDeTrabajo());
        model.addAttribute("cliente", clienteService.getAll());
        model.addAttribute("auditor", auditorService.getAll());

        return "agregarRegistroDeTrabajo";
    }

    @RequestMapping(value = "/agregarRegistroDeTrabajo", method = RequestMethod.POST)
    public String accionPaginaInsertar(RegistroDeTrabajo registroDeTrabajo, BindingResult result, Model model){
        //registroDeTrabajo.setCreated(Date.from(Instant.now()));
        registroDeTrabajoService.saveRegistroDeTrabajo(registroDeTrabajo);

        return "exito";
    }

    @RequestMapping(value = "/editarRegistroDeTrabajo/{id}")
    public String irAEditar(Model model, @PathVariable int id) {
        Optional<RegistroDeTrabajo> registroDeTrabajoToEdit = registroDeTrabajoService.getById(id);
        if (registroDeTrabajoToEdit.isPresent()){
            model.addAttribute("registroDeTrabajoToEdit", registroDeTrabajoToEdit);
            model.addAttribute("cliente", clienteService.getAll());
            model.addAttribute("auditor", auditorService.getAll());

            return "editarRegistroDeTrabajo";
        } else {
            return "notFound";
        }
    }

    @RequestMapping(value = "/editarRegistroDeTrabajo/{id}", method = RequestMethod.POST)
    public String guardarCambios(RegistroDeTrabajo registroDeTrabajo, BindingResult result,Model model,
                                 @PathVariable int id) {

        registroDeTrabajoService.updateRegistroDeTrabajo(registroDeTrabajo);
        return "exito";
    }


    @RequestMapping(value = "/borrarRegistroDeTrabajo/{id}")
    public String borrar(Model model, @PathVariable int id){
        registroDeTrabajoService.deleteRegistroDeTrabajo(id);

        return "exito";
    }
}
