package com.cenfotec.examen2.examen2.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String razonSocial;
    private String cedulaJuridica;
    private String direccion;
    private String telefono;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name ="PERSONADECONTACTO_ID", referencedColumnName = "ID")
    private List<PersonaDeContacto> personasDeContacto;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "cliente")
    private RegistroDeTrabajo registroDeTrabajo;

    public Cliente() {
    }

    public Cliente(String razonSocial, String cedulaJuridica, String direccion, String telefono) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.cedulaJuridica = cedulaJuridica;
        this.direccion = direccion;
        this.telefono = telefono;
        //this.personasDeContacto = personasDeContacto;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getRazonSocial() {
        return razonSocial;
    }
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCedulaJuridica() {
        return cedulaJuridica;
    }
    public void setCedulaJuridica(String cedulaJuridica) {
        this.cedulaJuridica = cedulaJuridica;
    }

    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<PersonaDeContacto> getPersonasDeContacto() {
        return personasDeContacto;
    }
    public void setPersonasDeContacto(List<PersonaDeContacto> personasDeContacto) {
        this.personasDeContacto = personasDeContacto;
    }
}
